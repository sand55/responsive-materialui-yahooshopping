import React from 'react';
import PropTypes from 'prop-types';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

export default class Ranking extends React.Component {

  componentWillMount() {
    this.props.onMount(this.props.categoryId);
  }

/***** componentWillReceiveProps の廃止対応
  componentWillReceiveProps(nextProps) {
    if(this.props.categoryId !== nextProps.categoryId) {
      this.props.onUpdate(nextProps.categoryId);
    }
  }
*****/

  // ** props.categoryId が更新されたときに呼ばれる。
  // ** actions.fetchRanking(categoryId)で、storeのcategoryとrankingを更新 ==> re-render() 
  componentDidUpdate(prevProps, prevState) {
    if(prevProps.categoryId !== this.props.categoryId) {
      this.props.onUpdate(this.props.categoryId);  // call: dispatch(actions.fetchRanking(categoryId));
    }
  }

  render() {
    const { category, ranking, error } = this.props;

    return (
      <div>
        <h2>{
          typeof category !== 'undefined'
          ? `${category.name}のランキング`
          : ''
        }</h2>

        {(() => {
          if(error) {
            return <p>エラーが発生しました。リロードしてください。</p>;
          } else if (typeof ranking === 'undefined') {
            return <p>読み込み中．．．</p>
          } else {
            return (
              <ol>
                {ranking.map((item, i) => (
                  <Card
                    key={`ranking-item-${item.code}`}
                    style={{ maxWidth: '400px', margin: '32 auto' }}
                  >
                    <CardMedia
                      image={item.imageUrl}
                      title={`${i+1}位 ${item.name}`}
                      style={{ height: '200px' }}
                    />
                    <CardContent>
                      <Typography type="title">
                        {`${i+1}位 ${item.name}`}
                      </Typography>
                    </CardContent>
                    <CardActions>
                      <Button
                        raised="true"
                        color="secondary"
                        fullWidth
                        href={item.url}
                      >商品のページへ</Button>
                    </CardActions>
                  </Card>
                ))}
              </ol>
            );
          }
        })()}
      </div>
    );
  }
}

Ranking.prpoTypes = {
  categoryId: PropTypes.string.isRequired,
  onMount: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,

  category: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }),
  ranking: PropTypes.arrayOf(
    PropTypes.shape({
      code: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired,
      imageUrl: PropTypes.string.isRequired
    })
  ),
  error: PropTypes.bool.isRequired
};

Ranking.defaultProps = {
  categoryId: '1'
};

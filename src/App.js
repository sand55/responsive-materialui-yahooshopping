import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import { Switch, Route, Redirect } from 'react-router-dom';
import Ranking from './containers/Ranking';
import Nav from './containers/Nav';

import { connect } from 'react-redux'

import { styles } from './Styles';


class ResponsiveDrawer extends React.Component {

  handleDrawerToggle = () => {
    this.props.dispatch({ type: 'MOBILE_TOGGLE' })
  };

  render() {
    const { classes, theme } = this.props;

console.log(JSON.stringify(styles(theme)))
console.log(theme.breakpoints.up('sm'))


/*
<AppBar position="fixed" にすると marginLeft: drawerWidth の値が正確に指定できる
<AppBar position="fixed" にすると AppBarはFlexのレイアウトから外れる。
残りのnavとmainの２要素でFlexのレイアウトを分け合う。
AppBarはstyleで指定されたwidthを占有する。

navは、AppBarのmarginLeft: drawerWidth, で空いたスペースに表示される。
navの横幅はwidth: drawerWidth,で、 flexShrink: 0,なのでブラウザサイズを変更しても縮むことはない。


mainは、<div className={classes.toolbar} />で、AppBarと重ならないようにスペースを作っている。
mainは、flexGrow: 1,で横幅を確保している。 flexGrowの要素はこれ一つなので全空きスペースを独占する。

*/


    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" noWrap>
              Yhoo!ショッピング
            </Typography>
          </Toolbar>
        </AppBar>
        <nav className={classes.drawer}>
          {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
          <Nav />
        </nav>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <div>
            <Switch>
              <Route path="/all" component={Ranking} />
              <Route path="/category/1" 
                render={ () => <Redirect to="/all" /> }
              />
              <Route path="/category/:id" 
                render={ ({match}) => <Ranking categoryId={match.params.id} /> }
              />
            </Switch>
          </div>
        </main>
      </div>
    );
  }
}

//          <div className={classes.toolbar} />


ResponsiveDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  // Injected by the documentation to work in an iframe.
  // You won't need it on your project.
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};

export default connect() ( withStyles(styles, { withTheme: true })(ResponsiveDrawer) );

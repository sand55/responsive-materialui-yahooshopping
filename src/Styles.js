const drawerWidth = 280;

export const styles = theme => ({
  root: {
    display: 'flex',
  },
  drawer: {
    // background: 'yellow',  // 効かない
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,  // Overrides - drawerPaper.width: drawerWidth と２重に指定する必要がある
      flexShrink: 0,  // !!! これが無いと main(content) が Drawer に重なってしまう
    },
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  // Override - Drawerの中で  classes={{ paper: classes.drawerPaper, }}
  drawerPaper: {
    width: drawerWidth,
    background: 'yellow', // 効果あり
  },
  content: {
    // marginLeft: drawerWidth,  // appBarに合わせる  画面幅を小さくすると無駄な空白ができる
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
});

import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import Nav from '../components/Nav';

const mapStateToProps = state => ({
  categories: state.shopping.categories,
  mobileOpen: state.Mobile.mobileOpen
});

const mapDispatchToProps = dispatch => ({
  onClick(path) {
    dispatch(push(path));  // ** push は actionの中で使う。dispatchが無いとダメ。
    dispatch({ type: 'MOBILE_FALSE' });
  },
  handleDrawerToggle() {
    dispatch({type: 'MOBILE_TOGGLE'})
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Nav)

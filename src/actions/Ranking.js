import fetchJsonp from 'fetch-jsonp';
import qs from 'qs';
// connected-react-router - action経由でルーティングが可能、push,replace..
import { replace, push } from 'connected-react-router';

const API_URL = 'https://shopping.yahooapis.jp/ShoppingWebService/V1/json/categoryRanking';

const APP_ID = 'dj00aiZpPXhiMWp2YjhpV2x1MiZzPWNvbnN1bWVyc2VjcmV0Jng9ODQ-';

const startRequest = category => ({
  type: 'START_REQUEST',
  payload: { category },
});


const receiveData = (category, error, response) => ({
  type: 'RECEIVE_DATA',
  payload: { category, error, response },
});


const finishRequest = category => ({
  type: 'FINISH_REQUEST',
  payload: { category },
});


// ** Redux-thunk
// ** action関数をラッピング - 内部で通常のactionを使いStoreを更新
export const fetchRanking = categoryId => {
  return async (dispatch, getState) => {
    const categories = getState().shopping.categories;
    const category = categories.find(category => (category.id === categoryId));
    if(typeof category === 'undefined') {
      dispatch(replace('/'));
      return;
    }

    // ** 通常のaction
    dispatch(startRequest(category));

    const queryString = qs.stringify({
      appid: APP_ID,
      category_id: categoryId,
    });
console.log(queryString)

    try {
      const response = await fetchJsonp(`${API_URL}?${queryString}`);
      const data = await response.json();
      // ** 通常のaction
      dispatch(receiveData(category, null, data));
    } catch (err) {
      // ** 通常のaction
      dispatch(receiveData(category, err));
    }
    // ** 通常のaction
    dispatch(finishRequest(category));

    // test
    /*****
    setTimeout( () => {
      // dispatch(replace('/'));
      dispatch(push('/'));
    }, 3000);
    *****/
  }
}


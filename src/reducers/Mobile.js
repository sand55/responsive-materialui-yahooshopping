const initialState = {
  mobileOpen: false
}

export default (state=initialState, action) => {
  switch (action.type) {
    case 'MOBILE_TOGGLE':
      return {
        mobileOpen: !state.mobileOpen
      };
    case 'MOBILE_FALSE':
      return state.mobileOpen
        ? { mobileOpen: false }
        : state ;
    default:
      return state;
  }
}
